﻿/// <reference path="scripts/typings/angularjs/angular.d.ts" />
/// <reference path="scripts/typings/jquery/jquery.d.ts" />
/// <reference path="scripts/typings/es6-promise/es6-promise.d.ts" />
/// <reference path="scripts/typings/openpgp/openpgp.d.ts" />

interface IMessage {
    message: string;
    id: string;
}

class ChatService {
    private messages: IMessage[];
    private topics: string[];
    constructor() {
        this.messages = [];
        this.topics = [];
    }

    public addMessage(message: IMessage): void {
        this.messages.push(message);
    }

    public setTopic(topics: string[]): void {
        if (topics.length > 0) {
            topics = topics.filter(function (value, index, array) :boolean {
                return value !== null && value !== "";
            });
        }
        this.topics = topics;
    }
}

class MainController {
    private showKeygen: boolean;
    private showTopic: boolean;
    private messageInput: string;
    private topicInput: string;
    private chatService: ChatService;
    constructor(cs: ChatService) {
        this.showKeygen = false;
        this.showTopic = true;
        this.messageInput = "";
        this.chatService = cs;
    }

    public messageChanged(keyEvent: JQueryKeyEventObject): void {
        if (keyEvent.which === 13 && this.messageInput.trim().length > 0) {
            this.chatService.addMessage({
                message: this.messageInput,
                id: this.generateUUID()
            });
            this.messageInput = "";
        }
    }

    public topicChanged(keyEvent: JQueryKeyEventObject): void {
        if (keyEvent.which === 13) {
            this.chatService.setTopic(this.topicInput.split(","));
            this.showTopic = false;
        }
    }

    private generateUUID(): string {
        var d = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
        return uuid;
    }
}

angular
    .module("enhide", [])
    .service("chatService", ChatService)
    .controller("MainController", [
    "chatService",
    MainController
]);