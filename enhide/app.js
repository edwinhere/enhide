var ChatService = (function () {
    function ChatService() {
        this.messages = [];
        this.topics = [];
    }
    ChatService.prototype.addMessage = function (message) {
        this.messages.push(message);
    };
    ChatService.prototype.setTopic = function (topics) {
        if (topics.length > 0) {
            topics = topics.filter(function (value, index, array) {
                return value !== null && value !== "";
            });
        }
        this.topics = topics;
    };
    return ChatService;
})();
var MainController = (function () {
    function MainController(cs) {
        this.showKeygen = false;
        this.showTopic = true;
        this.messageInput = "";
        this.chatService = cs;
    }
    MainController.prototype.messageChanged = function (keyEvent) {
        if (keyEvent.which === 13 && this.messageInput.trim().length > 0) {
            this.chatService.addMessage({
                message: this.messageInput,
                id: this.generateUUID()
            });
            this.messageInput = "";
        }
    };
    MainController.prototype.topicChanged = function (keyEvent) {
        if (keyEvent.which === 13) {
            this.chatService.setTopic(this.topicInput.split(","));
            this.showTopic = false;
        }
    };
    MainController.prototype.generateUUID = function () {
        var d = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
        return uuid;
    };
    return MainController;
})();
angular.module("enhide", []).service("chatService", ChatService).controller("MainController", [
    "chatService",
    MainController
]);
