﻿/// <reference path="../es6-promise/es6-promise.d.ts" />
declare var openpgp : openpgp.IOpenPGPStatic;
declare module openpgp {
    interface IOpenPGPStatic {
        key: openpgp.key.IKeyStatic;
        message: openpgp.message.IMessageStatic;
        encryptMessage(keys: openpgp.key.IKey[], text: string) : Promise<string>;
        decryptMessage(privateKey: openpgp.key.IKey, msg: openpgp.message.IMessage) : Promise<string>;
        generateKeyPair(options: {
            keyType: number;
            numBits: number;
            userId: string;
            passphrase: string;
            unlocked: boolean;
        }): Promise<{
            key: openpgp.key.IKey;
            privateKeyArmored: string;
            publicKeyArmored: string;
        }>;
    }
}
declare module openpgp.key {
    interface IKey {
        decrypt(passphrase: string): boolean;
    }
    interface IKeyStatic {
        Key: IKey;
        readArmored(armoredText: string): {
            keys: IKey[];
            err: Error[];
        };
    }
}

declare module openpgp.message {
    interface IMessage {
    }

    interface IMessageStatic {
        Message: IMessage;
        readArmored(armoredText: string): IMessage;
    }
}